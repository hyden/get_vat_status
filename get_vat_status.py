import argparse
import time

import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def get_vat_status(nip, path):
    driver_options = {}
    if path:
        driver_options['executable_path'] = path

    driver = webdriver.PhantomJS(**driver_options)

    driver.get('https://ppuslugi.mf.gov.pl/_/?link=VAT')

    time.sleep(1)

    link = driver.find_element_by_css_selector('.SidebarLinkChVAT')
    link.click()

    time.sleep(1)

    input = driver.find_element_by_css_selector('#b-7')
    input.send_keys(str(nip))
    input.send_keys(Keys.ENTER)

    time.sleep(3)

    button = driver.find_element_by_css_selector('#b-8')
    button.click()

    time.sleep(2)

    container = driver.find_element_by_id('container_b-3')

    response = str(container.text)

    status = response.find('VAT czynny') != -1
    return status

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='NIP -> VAT status mapping')
    parser.add_argument('nip', type=int, help='NIP number')
    parser.add_argument('--path', type=str, nargs='?', help='PhantomJS executable path')

    opts = parser.parse_args()

    print(int(get_vat_status(opts.nip, opts.path)))

    sys.exit(0)
